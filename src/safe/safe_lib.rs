// This is a frankeinstein of the simulate_browser and nfs-api from Maidsafe
// safe_client_libs
//
// https://github.com/maidsafe/safe_client_libs
//
// Copyright 2017 wrnice under the GPL licence

// Original Maidsafe Licence follows :

// Copyright 2015 MaidSafe.net limited.
//
// This SAFE Network Software is licensed to you under (1) the MaidSafe.net
// Commercial License,
// version 1.0 or later, or (2) The General Public License (GPL), version 3,
// depending on which
// licence you accepted on initial access to the Software (the "Licences").
//
// By contributing code to the SAFE Network Software, or to this project
// generally, you agree to be
// bound by the terms of the MaidSafe Contributor Agreement, version 1.0.
// This, along with the
// Licenses can be found in the root directory of this project at LICENSE,
// COPYING and CONTRIBUTOR.
//
// Unless required by applicable law or agreed to in writing, the SAFE Network
// Software distributed
// under the GPL Licence is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
// OR CONDITIONS OF ANY
// KIND, either express or implied.
//
// Please review the Licences for the specific language governing permissions
// and limitations
// relating to use of the SAFE Network Software.

use chan;
use routing::XOR_NAME_LEN;
use safe_core::core::client::Client;
use safe_core::dns::dns_operations::DnsOperations;
use safe_core::dns::errors::DnsError;
use safe_core::nfs::{self, AccessLevel};
use safe_core::nfs::directory_listing::DirectoryListing;
use safe_core::nfs::errors::NfsError;
use safe_core::nfs::helper::directory_helper::DirectoryHelper;
use safe_core::nfs::helper::file_helper::FileHelper;
use safe_core::nfs::helper::writer::Mode;
use std::io::{self, Read};
use std::sync::{Arc, Mutex};
use time;


pub fn login_account() -> Result<Client, NfsError> {
    let mut secret_0 = String::new();
    let mut secret_1 = String::new();

    println!("\n\tLogin");
    println!("\t================");

    println!("\n------------ Enter account-locator ---------------");
    let _ = io::stdin().read_line(&mut secret_0);
    secret_0 = secret_0.trim().to_string();

    println!("\n------------ Enter password ---------------");
    let _ = io::stdin().read_line(&mut secret_1);
    secret_1 = secret_1.trim().to_string();

    // Log into the account
    println!("\nTrying to log into the created account using supplied credentials ...");
    let client = Client::log_in(&secret_0, &secret_1)?;
    println!("Account Login Successful !!");
    Ok(client)
}

fn get_root_directory(client: Arc<Mutex<Client>>) -> Result<DirectoryListing, NfsError> {
    let directory_helper = DirectoryHelper::new(client.clone());
    directory_helper.get_user_root_directory_listing()
}

fn update_home_page(client: Arc<Mutex<Client>>,
                    dns_operations: &DnsOperations)
                    -> Result<(), DnsError> {
    let long_name = "now";
    let service_name = "time";

    println!("Fetching data...");

    let dir_key = dns_operations.get_service_home_directory_key(&long_name, &service_name, None)?;
    let directory_helper = DirectoryHelper::new(client.clone());
    let dir_listing = directory_helper.get(&dir_key)?;

    let file = dir_listing.get_files()
        .iter()
        .find(|a| *a.get_name() == "index.html".to_string())
        .ok_or(DnsError::Unexpected("Could not find homepage !!".to_string()))?;

    let hour = time::now_utc().tm_hour.to_string();
    let min = time::now_utc().tm_min.to_string();
    let mesg = "Time is now : ".to_string() + &hour + ":" + &min +
               " ... refresh the page to see time passing ...";
    let data = mesg.into_bytes();
    let mut file_helper = FileHelper::new(client.clone());
    let mut writer =
        file_helper.update_content(file.clone(), Mode::Overwrite, dir_listing.clone())?;
    writer.write(&data[..])?;
    let _ = writer.close()?;
    println!("File Updated");

    let mut file_helper2 = FileHelper::new(client.clone());
    let mut reader = file_helper2.read(file)?;
    let size = reader.size();
    let content = reader.read(0, size)?;

    println!("\n-----------------------------------------------------");
    println!("                 Home Page Contents");
    println!("-----------------------------------------------------\n");
    println!("{}",
             try!(String::from_utf8(content).map_err(|_| {
                 DnsError::Unexpected("Cannot convert contents to displayable string !!"
                     .to_string())
             })));

    Ok(())
}

#[macro_use(unwrap, chan)]
fn safe_save() {
    let tick = chan::tick_ms(60000);

    let test_client = unwrap!(login_account());
    let client = Arc::new(Mutex::new(test_client));

    println!("\n\t-- Preparing storage ----\n");
    let mut root_directory = unwrap!(get_root_directory(client.clone()));

    println!("Initialising Dns...");
    let dns_operations = unwrap!(DnsOperations::new(client.clone()));

    loop {
        chan_select! {
            tick.recv() => {
				println!("tick") ;

				if let Err(err) = update_home_page(client.clone(),
                                                                  &dns_operations) {
                        println!("\nFailed !!");
				}

			},
        }
    }
}
